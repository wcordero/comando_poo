<?php

class Person{
    private $firstName;
    private $lastName;
    private $nickName;

    public function __construct($firstName, $lastName){
        $this->firstName= $firstName;
        $this->lastName = $lastName;
    }

    public function setNickName($nickname){
        $this->nickName = $nickname;
    }

    public function getFirstName(){
        return $this->firstName;
    }

    public function getLastName(){
        return $this->lastName;
    }

    public function getNickName(){
        return $this->nickName;
    }

    public function fullName(){
        return $this->firtName. ' ' . $this->lastName;
    }
}

$person1 = new Person('Walter', 'Cordero');

echo $person1->fullName();